package com.company;

import java.time.LocalDateTime;

/**
 * Created by konsk_000 on 14.10.2016.
 */
public class Logger {
    String name;
    Type type;
    String message;

    public Logger(String name){
        this.name=name;
    }

    public void addMessage(Type type, String message, String... args){
        StringBuilder sb = new StringBuilder();
        sb.append(LocalDateTime.now());
        sb.append(" ");
        sb.append(name);
        sb.append(type.toString());
        sb.append(" ");
        sb.append(message);
        sb.append("\n");

        System.out.printf(sb.toString(), args);
    }

}
