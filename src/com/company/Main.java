package com.company;

public class Main {

    public static void main(String[] args) {
        Logger firstLogger = new Logger("FIRST");
        Logger secondLogger = new Logger("SECOND");

        firstLogger.addMessage(Type.INFO, "Param %s", "1");
        secondLogger.addMessage(Type.ERROR, "Param %s and Param 2 %s","1", "2");

    }
}
