package com.company;

/**
 * Created by konsk_000 on 14.10.2016.
 */
public enum Type {
    INFO("[INFO]"),
    WARN("[WARN]"),
    ERROR("[ERROR]");

    private String name;
    private Type (String name){
        this.name=name;
    }

    public String toString(){
        return name;
    }
}
